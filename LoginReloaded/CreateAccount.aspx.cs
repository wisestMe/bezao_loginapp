﻿using LoginReloaded.Control;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace LoginReloaded
{
    public partial class CreateAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            usernameUsed.Visible = false;
            emailUsed.Visible = false;
            passwordError.Visible = false;
        }
        
        public string UserName { get; set; }
        public void buttonCreate_Click(object sender, EventArgs e)
        {
            LoginReloaded.Control.ControlDB dbControl = new Control.ControlDB();
            dbControl.OpenDB();

            //UserName = username.Text;
            //add user to db if username does not already exist then add
            //string query = "SELECT COUNT(*) FROM Users WHERE Username=@Username";
            //SqlCommand checkUserName = new SqlCommand(query, dbControl.connectDB);
            //checkUserName.Parameters.AddWithValue("@Username", username.Text.Trim());
            //int count = Convert.ToInt32(checkUserName.ExecuteScalar());
            Gateman userCheck = new Gateman();
            int usernameCount = userCheck.IsUsernameAvailable(username.Text);
            int emailCount = userCheck.IsEmailAvailable(email.Text);
            bool IsPasswordSame = userCheck.SamePassword(password.Text, passwordConfirm.Text);

            if(IsPasswordSame)
            {
                if (emailCount < 1)
                {
                    if (usernameCount < 1)
                    {
                        string queryInsert = "INSERT INTO Users (Username, Password, email) VALUES('" + username.Text.Trim() + "', '" + password.Text.Trim() + "', '" + email.Text.Trim() + "')";
                        SqlCommand userCreate = new SqlCommand(queryInsert, dbControl.connectDB);
                        userCreate.ExecuteNonQuery();
                        Response.Redirect("UserLogin.aspx");
                    }
                    else
                    {
                        usernameUsed.Visible = true;
                    }
                }
                else
                {
                    emailUsed.Visible = true;
                }
            }
            else
            {
            passwordError.Visible = true;
            }
        }
    }
}