﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace LoginReloaded
{
    public partial class UserLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loginErrorMessage.Visible = false;
        }

        protected void buttonLogin_Click(object sender, EventArgs e)
        {
            LoginReloaded.Control.ControlDB dbControl = new Control.ControlDB();
            dbControl.OpenDB();

            string query = "SELECT COUNT(1) FROM Users WHERE Username=@Username AND Password=@Password";
            SqlCommand sqlInstruction = new SqlCommand(query, dbControl.connectDB);

            sqlInstruction.Parameters.AddWithValue("@Username", textUserName.Text.Trim());
            sqlInstruction.Parameters.AddWithValue("@Password", textPassword.Text.Trim());

            int count = Convert.ToInt32(sqlInstruction.ExecuteScalar());
            if (count == 1)
            {
                Session["Username"] = textUserName.Text.Trim();
                Response.Redirect("UserPage.aspx");
            }
            else
            {
                loginErrorMessage.Visible = true;
            }
            
        }
    }
}