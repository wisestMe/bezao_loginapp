﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserLogin.aspx.cs" Inherits="LoginReloaded.UserLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Username"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="textUserName" runat="server"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="textPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>
                        
                    </td>
                    <td>
                        <asp:Button ID="buttonLogin" runat="server" Text="Login" OnClick="buttonLogin_Click" /> 
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Label ID="loginErrorMessage" runat="server" Text="Incorrect login details" ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                        
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
