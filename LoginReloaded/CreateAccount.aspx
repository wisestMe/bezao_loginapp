﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateAccount.aspx.cs" Inherits="LoginReloaded.CreateAccount" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="labelFirstName" runat="server" Text="First Name"></asp:Label>

                    </td>
                    <td>
                        <asp:TextBox ID="textFirstName" runat="server"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Label ID="labelLasttName" runat="server" Text="Last Name"></asp:Label>

                    </td>
                    <td>
                        <asp:TextBox ID="textLastName" runat="server"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Label ID="labelUserName" runat="server" Text="Username"></asp:Label>

                    </td>
                    <td>
                        <asp:TextBox ID="username" runat="server"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Label ID="labelPassword" runat="server" Text="Password"></asp:Label>

                    </td>
                    <td>
                        <asp:TextBox ID="password" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Label ID="labelPasswordConfirm" runat="server" Text="Confirm Password"></asp:Label>

                    </td>
                    <td>
                        <asp:TextBox ID="passwordConfirm" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Label ID="labelEmail" runat="server" Text="Email"></asp:Label>

                    </td>
                    <td>
                        <asp:TextBox ID="email" runat="server"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Button ID="buttonCreate" runat="server" Text="Create" OnClick="buttonCreate_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="usernameUsed" runat="server" Text="Sorry, username has been taken" ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                         <asp:Label ID="emailUsed" runat="server" Text="Sorry, Email is already registered" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="passwordError" runat="server" Text="Sorry, password mismatch." ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                         
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
