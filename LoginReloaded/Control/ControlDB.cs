﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LoginReloaded.Control;
using System.Data.SqlClient;
using System.Data;


namespace LoginReloaded.Control
{
    public class ControlDB
    {
        public SqlConnection connectDB { get; set; }
        public void OpenDB()
        {
            connectDB = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=WebLogin;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            connectDB.Open();
        }
    }
}