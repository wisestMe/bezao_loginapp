﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace LoginReloaded.Control
{
    public class Gateman
    {
        LoginReloaded.Control.ControlDB dbControl = new Control.ControlDB();
        CreateAccount account = new CreateAccount();
        public int IsUsernameAvailable(string accountHolder)
        {
            dbControl.OpenDB();
            //CreateAccount account = new CreateAccount();
            //add user to db if username does not already exist then add
            string query = "SELECT COUNT(*) FROM Users WHERE Username=@Username";
            SqlCommand checkUserName = new SqlCommand(query, dbControl.connectDB);
            checkUserName.Parameters.AddWithValue("@Username", accountHolder);
            int count = Convert.ToInt32(checkUserName.ExecuteScalar());
            return count;
        }

        public int IsEmailAvailable(string accountHolder)
        {
            dbControl.OpenDB();       
            //add user to db if email does not already exist then add
            string query = "SELECT COUNT(*) FROM Users WHERE email=@email";
            SqlCommand checkEmail = new SqlCommand(query, dbControl.connectDB);
            checkEmail.Parameters.AddWithValue("@email", accountHolder);
            int count = Convert.ToInt32(checkEmail.ExecuteScalar());
            return count;
        }

        bool PasswordSame;
        public bool SamePassword(string PasswordOne, string PasswordTwo)
        {
            

            if(PasswordOne == PasswordTwo)
            {
                PasswordSame = true;
            }
            return PasswordSame;
        }
    }
}